/*
*create a .sol contract for accounting system
1) contains usestype like admin,tokenholder,and owner
2) token must be erc20 token
3) contains account info for tokenholders
account
firstName
lastName
userType
4) make use of contract with all high level contract structure like - event,address,mapping,struct,enum,constructor,function,address mapping
5) user can transfer from, to adress with amount
6) admin can froze account of users
7) provide modifier to all the function with usertype
8) register account for new token holder
9) validate and checklist of account and mint the token for token holder
*/








pragma solidity ^0.8.0;


abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Returns the amount of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the amount of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves `amount` tokens from the caller's account to `recipient`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address recipient, uint256 amount) external returns (bool);

    
    function allowance(address owner, address spender) external view returns (uint256);

    function approve(address spender, uint256 amount) external returns (bool);

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) external returns (bool);

    
    event Transfer(address indexed from, address indexed to, uint256 value);

    
    event Approval(address indexed owner, address indexed spender, uint256 value);
}


pragma solidity ^0.8.0;



interface IERC20Metadata is IERC20 {
    /**
     * @dev Returns the name of the token.
     */
    function name() external view returns (string memory);

    /**
     * @dev Returns the symbol of the token.
     */
    function symbol() external view returns (string memory);

    /**
     * @dev Returns the decimals places of the token.
     */
    function decimals() external view returns (uint8);
}

// File: @openzeppelin/contracts/token/ERC20/ERC20.sol


// OpenZeppelin Contracts v4.4.0 (token/ERC20/ERC20.sol)

pragma solidity ^0.8.0;





contract ERC20 is Context, IERC20, IERC20Metadata {
    mapping(address => uint256) private _balances;

    mapping(address => mapping(address => uint256)) private _allowances;

    uint256 private _totalSupply;

    string private _name;
    string private _symbol;

    constructor(string memory name_, string memory symbol_) {
        _name = name_;
        _symbol = symbol_;
    }

    /**
     * @dev Returns the name of the token.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev Returns the symbol of the token, usually a shorter version of the
     * name.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    /**
     * @dev See {IERC20-totalSupply}.
     */
    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    /**
     * @dev See {IERC20-balanceOf}.
     */
    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][_msgSender()];
        require(currentAllowance >= amount, "ERC20: transfer amount exceeds allowance");
        unchecked {
            _approve(sender, _msgSender(), currentAllowance - amount);
        }

        return true;
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");

        _beforeTokenTransfer(sender, recipient, amount);

        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount, "ERC20: transfer amount exceeds balance");
        unchecked {
            _balances[sender] = senderBalance - amount;
        }
        _balances[recipient] += amount;

        emit Transfer(sender, recipient, amount);

        _afterTokenTransfer(sender, recipient, amount);
    }

    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: mint to the zero address");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);

        _afterTokenTransfer(address(0), account, amount);
    }

    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: burn from the zero address");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount, "ERC20: burn amount exceeds balance");
        unchecked {
            _balances[account] = accountBalance - amount;
        }
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);

        _afterTokenTransfer(account, address(0), amount);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}
}






contract Accounts is ERC20{

    //using DSMath for uint256;
    address owner;

    enum TYPE{ TOKENHOLDER,ADMIN, OWNER} //defining an enum for userType

    struct accountInfos{
        address account;
        string firstName;
        string lastName;
        TYPE type1;
    } //defining struct for account Informations

    accountInfos[] public accounts; //array to store all the account infomrations
    mapping(address=>bool) public blackListed; //to check which user is blacklisted
    mapping(address=>uint) public accountsIndex; //to get the index of an account holder's position in the accounts array
    mapping(address=>uint) public userBalance; //To fetch the balance of each user


    modifier ownertype(){
        //require(accountsIndex[msg.sender]==0);
       //accountInfos storage acc = accounts[accountsIndex[msg.sender]]; 
       require(msg.sender == owner, "Only owner is allowed");
       _;
    }
    
    modifier admintype(){
        accountInfos memory acc = accounts[accountsIndex[msg.sender]];
        require(acc.type1 == TYPE.ADMIN, "Only admin is allowed");
        _;
    }

    
    modifier holdertype(TYPE _state){
        accountInfos memory acc = accounts[accountsIndex[msg.sender]];
        require(acc.type1 == TYPE.TOKENHOLDER,"Only registered users are allowed");
        _;
    }

    /*
        Constructor takes arguments for firstname and lastname for the owner of the contract
        and mints him some minimum amount of tokens
    */
        
    constructor(string memory _firstname, string memory _lastname) ERC20("Appscrip","XAP") {
        accounts.push(accountInfos(msg.sender,_firstname,_lastname,TYPE.OWNER));
        uint id = accounts.length-1;
        owner=msg.sender;
        _mint(msg.sender, 1000000 * 10 ** decimals());
        accountsIndex[msg.sender]=id;
        userBalance[msg.sender]=ERC20.balanceOf(msg.sender);
    } 

    /*
        This function allows owner to create admin for their accounting system
        Only owner is able to initiate this transaction
    */

    function makeAdmin(address _address,string memory _firstname, string memory _lastname) public ownertype{
        accounts.push(accountInfos(_address,_firstname,_lastname,TYPE.ADMIN));
        uint id = accounts.length - 1;
        accountsIndex[msg.sender]=id;
        userBalance[msg.sender]=ERC20.balanceOf(msg.sender);
    }

    //User can be blacklisted only by admins 
    function blackListUser(address _targetId) public admintype{
        blackListed[_targetId]=true;
    }

    /*
        Function to create a new user
        Admins can only register users in the system
    */

    function createUser(address _address,string memory _firstname, string memory _lastname,uint mintAmount) public admintype{
        require(accountsIndex[_address]!=0, "User already registered");
        accounts.push(accountInfos(msg.sender,_firstname,_lastname,TYPE.TOKENHOLDER));
        uint id = accounts.length -1;
        _mint(_address, mintAmount);
        accountsIndex[msg.sender]=id;
        userBalance[msg.sender]=ERC20.balanceOf(msg.sender);
    }



    /*
        Using this function to do basic checks before sending a transfer transaction to the system
    */

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        require(blackListed[from]==false, "Sender is balcklisted");
        require(blackListed[to]==false,"Recipient is blacklisted");
        super._beforeTokenTransfer(from, to, amount);

        // do stuff before every transfer
        // e.g. check that users (other than when minted) 
        // can receive or send tokens
    }

    

}



